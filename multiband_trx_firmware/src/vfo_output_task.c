/*
 * normal_vfo_output_task.c
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */


#include "hardware.h"
#include "vfo_output_task.h"
#include "system_constants.h"
#include "si5351.h"
#include "vfo_control.h"

static SYSTEM_STATUS_t*status;
static VFO_CONFIG_t previous_vfo_config;
static RADIO_CIRCUIT_POWER_STATE_t previous_radio_circuit_state;
static CALIBRATION_DATA_t previous_calibration_state;

void copy_state(SYSTEM_STATUS_t*origin,VFO_CONFIG_t*vfo,RADIO_CIRCUIT_POWER_STATE_t*radio,CALIBRATION_DATA_t*cal){
	if(origin->active_vfo==0){
		*vfo=origin->vfo_a;
	}else{
		*vfo=origin->vfo_b;
	}
	*radio=origin->radio_circuit_status;
	*cal=origin->calibration;
}


void vfo_output_task_init(SYSTEM_STATUS_t*s){
	status=s;
	copy_state(s,&previous_vfo_config,&previous_radio_circuit_state,&previous_calibration_state);
	apply_vfo_state(&previous_vfo_config,&previous_radio_circuit_state,&previous_calibration_state);
}
void vfo_output_task(){
	VFO_CONFIG_t vfo;
	RADIO_CIRCUIT_POWER_STATE_t radio;
	CALIBRATION_DATA_t cal;
	copy_state(status,&vfo,&radio,&cal);
	if((vfo_config_compare(&vfo,&previous_vfo_config)!=0)
			||(radio_circuit_power_state_compare(&radio,&previous_radio_circuit_state)!=0)
			||(calibration_data_compare(&cal,&previous_calibration_state)!=0)){
		apply_vfo_state(&vfo,&radio,&cal);
		previous_vfo_config=vfo;
		previous_radio_circuit_state=radio;
		previous_calibration_state=cal;

	}
}
