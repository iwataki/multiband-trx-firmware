/*
 * power_management_task.c
 *
 *  Created on: 2020/03/18
 *      Author: 宗一郎
 */

#include "power_management_task.h"
#include "power_management.h"
#include "si5351.h"
#include "aqm1248.h"
#include "system_state_store.h"
#include "config_memory.h"
#include "hardware.h"
#include "system_constants.h"
#include "vfo_control.h"
#include <util/delay.h>

static USERSW_t*power_sw;
static SYSTEM_STATUS_t*status;

void init_power_management_task(SYSTEM_STATUS_t*s,USERSW_t*power_sw_){
	power_sw=power_sw_;
	status=s;
}
void power_management_task(void){
	int switch_event=get_switch_event(power_sw);
	if(switch_event==USERSW_RELEASE){//vr switch off
		SystemNonvolatileConfig_t config;
		copy_system_status(&config);
		save_config(&config);
		digitalout_reset(&receiver_enable_pin);
		digitalout_reset(&transmitter_enable_pin);
		system_poweroff();
		aqm1248_sleep();
		system_sleep();//スイッチオンされるまでずっとパワーダウンで待機
		system_poweron();
		_delay_ms(1);
		init_si5351();
		aqm1248_wakeup();
		if(status->radio_circuit_status.is_receiver_enable){
			digitalout_set(&receiver_enable_pin);

		}
		if(status->radio_circuit_status.is_transmitter_enable){
			digitalout_set(&transmitter_enable_pin);
		}
		if(status->active_vfo==0){
			apply_vfo_state(&(status->vfo_a),&(status->radio_circuit_status),&(status->calibration));
		}else{
			apply_vfo_state(&(status->vfo_b),&(status->radio_circuit_status),&(status->calibration));
		}
	}
}
