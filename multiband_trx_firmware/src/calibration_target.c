/*
 * calibration_target.c
 *
 *  Created on: 2020/03/20
 *      Author: �@��Y
 */

#include "calibration_target.h"
#include "task.h"
uint8_t get_cal_target(TASK_LIST_t*t,APPLICATION_TASK_ID_TABLE_t*idt){
	if(get_task_status(t,idt->if_calibration_encoder_task_id)==TASK_ACTIVE){
		return CAL_TARGET_IF;
	}else {
		return CAL_TARGET_XTAL;
	}
}
void set_cal_target(TASK_LIST_t*t,APPLICATION_TASK_ID_TABLE_t*idt,uint8_t cal_target){
	if(cal_target==CAL_TARGET_IF){
		deactivate_task(t,idt->xtal_calibration_encoder_task_id);
		activate_task(t,idt->if_calibration_encoder_task_id);
	}else{
		activate_task(t,idt->xtal_calibration_encoder_task_id);
		deactivate_task(t,idt->if_calibration_encoder_task_id);
	}
}
