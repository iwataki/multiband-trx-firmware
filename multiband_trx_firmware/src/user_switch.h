/*
 * user_switch.h
 *
 *  Created on: 2016/11/01
 *      Author: �@��Y
 */

#ifndef USER_SWITCH_H_
#define USER_SWITCH_H_
#include <inttypes.h>
#include <avr/io.h>

#define USERSW_RELEASE 0
#define USERSW_HOLD 1
#define USERSW_PUSHED 2
#define USERSW_RELEASED 3
#define USERSW_NO_EVENT 4

typedef struct{
	volatile uint8_t*PIN;
	volatile uint8_t*DDR;
	volatile uint8_t*PORT;
	volatile uint8_t bit;
	volatile uint8_t latest_state;
	volatile uint8_t previous_state;
	volatile uint8_t latest_event;
}USERSW_t;

void init_usersw(USERSW_t*sw);
void update_switch(USERSW_t*sw);
int get_switch(USERSW_t*sw);
int get_switch_event(USERSW_t*sw);


#endif /* USER_SWITCH_H_ */
