/*
 * rssi_indicator.h
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */

#ifndef RSSI_INDICATOR_H_
#define RSSI_INDICATOR_H_
#include <inttypes.h>
typedef struct{
	uint8_t x;
	uint8_t y;
}RSSI_INDICATOR_t;

void rssi_indicator_create(RSSI_INDICATOR_t*r,uint8_t x,uint8_t y);
//rssi range:0~9 (S1,S2,S3,S4,S5,S6,S7,S8,S9,+ ++)
void rssi_indicator_setvalue(RSSI_INDICATOR_t*r,uint8_t rssi);

#endif /* RSSI_INDICATOR_H_ */
