/*
 * calibration_view_task.h
 *
 *  Created on: 2020/03/20
 *      Author: �@��Y
 */

#ifndef CALIBRATION_VIEW_TASK_H_
#define CALIBRATION_VIEW_TASK_H_

#include "data_structure.h"
#include "textbox.h"
#include "rssi_indicator.h"
#include "application.h"


typedef struct{
	TEXTBOX_t calibration_target;
	TEXTBOX_t frequency;
	RSSI_INDICATOR_t rssi;
}CALIBRATION_MODE_VIEW_t;

typedef struct{
	uint8_t calibration_target;
	CALIBRATION_DATA_t calibration_data;
	uint16_t rssi;
}CALIBRATION_MODE_VIEW_DATA_t;

void calibration_view_task_init(SYSTEM_STATUS_t*s,TASK_LIST_t*t,APPLICATION_TASK_ID_TABLE_t*ids);
void calibration_view_task_on_activated(void);
void calibration_view_task(void);

#endif /* CALIBRATION_VIEW_TASK_H_ */
