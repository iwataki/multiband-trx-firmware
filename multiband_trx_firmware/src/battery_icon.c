/*
 * battery_icon.c
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */


#include "battery_icon.h"

#include "aqm1248.h"

#include <avr/pgmspace.h>

const uint8_t battery_icon_data[]PROGMEM={
		0x81,0x7e,0x66,0x5a,0x5a,0x66,0x7e,0x99,
		0x00,0x7e,0x7e,0x42,0x42,0x42,0x7e,0x99,
		0x00,0x7e,0x7e,0x7e,0x42,0x42,0x7e,0x99,
		0x00,0x7e,0x7e,0x7e,0x7e,0x42,0x7e,0x99,
		0x00,0x7e,0x7e,0x7e,0x7e,0x7e,0x7e,0x99,
};

void battery_icon_create(BATTERY_ICON_t*icon,uint8_t x,uint8_t y){
	icon->x=x;
	icon->y=y;
}
void battery_icon_setvalue(BATTERY_ICON_t*icon,uint8_t level){
	if(level>BATTERY_ICON_FULL){
		return;
	}
	uint8_t icon_data[8];
	for(uint8_t i=0;i<8;i++){
		icon_data[i]=pgm_read_byte(battery_icon_data+level*8+i);
	}
	draw_icon(icon->x,icon->y,1,icon_data,8);
}
