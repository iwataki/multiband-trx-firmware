/*
 * ptt_observe_calibration_mode_task.h
 *
 *  Created on: 2020/03/20
 *      Author: �@��Y
 */

#ifndef PTT_OBSERVE_CALIBRATION_MODE_TASK_H_
#define PTT_OBSERVE_CALIBRATION_MODE_TASK_H_

#include "user_switch.h"
#include "data_structure.h"

void ptt_observe_calibration_mode_task_init(SYSTEM_STATUS_t*s,USERSW_t*ptt);
void ptt_observe_calibration_mode_task(void);
void ptt_observe_calibration_mode_task_on_activated(void);

#endif /* PTT_OBSERVE_CALIBRATION_MODE_TASK_H_ */
