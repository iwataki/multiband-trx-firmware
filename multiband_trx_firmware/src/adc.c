/*
 * adc.c
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */

#include <avr/io.h>
#include "adc.h"

void adc_init(ADC_t*adc,uint8_t ch){
	if(ch>0x0f){
		return;
	}
	adc->ch=ch;
	ADCSRA|=0x85;//adc enable, clock=fcpu/32=500kHz

}
uint16_t adc_get(ADC_t*adc){
	uint8_t admux_temp=ADMUX;
	admux_temp&=0xf0;//clear adc input mux
	admux_temp|=adc->ch;
	ADMUX=admux_temp;
	ADCSRA|=(1<<6);//start conversion
	while((ADCSRA&(1<<6))!=0);//wait for complete
	uint16_t adc_raw_value=ADCW;
	uint32_t voltage=((uint32_t)adc_raw_value)*5000/1024;
	return (uint16_t)voltage;
}
