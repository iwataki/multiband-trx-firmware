/*
 * calibration_target.h
 *
 *  Created on: 2020/03/20
 *      Author: �@��Y
 */

#ifndef CALIBRATION_TARGET_H_
#define CALIBRATION_TARGET_H_
#include "application.h"

#define CAL_TARGET_XTAL 0
#define CAL_TARGET_IF 1

uint8_t get_cal_target(TASK_LIST_t*t,APPLICATION_TASK_ID_TABLE_t*idt);
void set_cal_target(TASK_LIST_t*t,APPLICATION_TASK_ID_TABLE_t*idt,uint8_t cal_target);

#endif /* CALIBRATION_TARGET_H_ */
