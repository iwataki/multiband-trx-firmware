#ifndef __SOFTENC_H__
//IOポートから低速のロータリエンコーダを読む
#define __SOFTENC_H__

#include <inttypes.h>
#include <avr/io.h>
//A相B相のつながっているピンを指定
/*#define DDR_PHASE_A DDRD
#define DDR_PHASE_B DDRD

#define PIN_PHASE_A	PIND
#define PIN_PHASE_B PIND

#define PORT_PHASE_A PORTD
#define PORT_PHASE_B PORTD

#define PHASE_A 0
#define PHASE_B 1*/

typedef struct{
	int16_t prev_count;
	int16_t latest_count;
	volatile uint8_t*DDR_phaseA;
	volatile uint8_t*DDR_phaseB;
	volatile uint8_t*PIN_phaseA;
	volatile uint8_t*PIN_phaseB;
	volatile uint8_t*PORT_phaseA;
	volatile uint8_t*PORT_phaseB;
	volatile uint8_t bitA;
	volatile uint8_t bitB;
	volatile int8_t direction;
	volatile uint8_t prev_portval;
	volatile int8_t prev_direction;
}ENCODER_t;


void init_enc(ENCODER_t*enc,int dir);
void set_encoder_val(ENCODER_t*enc,int val);
int get_encoder_val(ENCODER_t*enc);
int get_encoder_diff(ENCODER_t*enc);
void update_encoder(ENCODER_t*enc);
#endif
