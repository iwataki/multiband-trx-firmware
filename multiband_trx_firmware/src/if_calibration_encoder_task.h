/*
 * if_calibration_encoder_task.h
 *
 *  Created on: 2020/03/20
 *      Author: �@��Y
 */

#ifndef IF_CALIBRATION_ENCODER_TASK_H_
#define IF_CALIBRATION_ENCODER_TASK_H_

#include "data_structure.h"

void if_calibration_encoder_task_init(SYSTEM_STATUS_t*s);
void if_calibration_encoder_task();



#endif /* IF_CALIBRATION_ENCODER_TASK_H_ */
