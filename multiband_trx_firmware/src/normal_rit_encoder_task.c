/*
 * normal_rit_encoder_task.c
 *
 *  Created on: 2020/03/20
 *      Author: �@��Y
 */


#include "normal_rit_encoder_task.h"
#include "hardware.h"
#include "system_constants.h"

static SYSTEM_STATUS_t*status;
static int diff_incr=0;
void normal_rit_encoder_task_init(SYSTEM_STATUS_t*s){
	status=s;
}
void normal_rit_encoder_task(){
	diff_incr+=get_encoder_diff(&encoder);
	int diff_half=diff_incr/2;
	if(diff_half!=0){
		diff_incr=0;
	}
	if(status->active_vfo==0){
		status->vfo_a.rit_config.frequency+=100*diff_half;
		if(status->vfo_a.rit_config.frequency>RIT_FREQ_MAX){
			status->vfo_a.rit_config.frequency=RIT_FREQ_MAX;
		}
		if(status->vfo_a.rit_config.frequency<RIT_FREQ_MIN){
			status->vfo_a.rit_config.frequency=RIT_FREQ_MIN;
		}
	}else{
		status->vfo_b.rit_config.frequency+=100*diff_half;
		if(status->vfo_b.rit_config.frequency>RIT_FREQ_MAX){
			status->vfo_b.rit_config.frequency=RIT_FREQ_MAX;
		}
		if(status->vfo_b.rit_config.frequency<RIT_FREQ_MIN){
			status->vfo_b.rit_config.frequency=RIT_FREQ_MIN;
		}
	}
}
