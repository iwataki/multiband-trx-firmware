/*
 * data_structure.h
 *
 *  Created on: 2020/03/16
 *      Author: 宗一郎
 */

#ifndef DATA_STRUCTURE_H_
#define DATA_STRUCTURE_H_
#include <inttypes.h>

//RITの状態を表す構造体
typedef struct {
	uint8_t is_enable;
	int16_t frequency;
}RIT_CONFIG_t;

uint8_t rit_config_compare(RIT_CONFIG_t*d1,RIT_CONFIG_t*d2);

//VFOの状態を表す構造体
typedef struct{
	uint16_t step;
	uint32_t frequency;
	RIT_CONFIG_t rit_config;
}VFO_CONFIG_t;
uint8_t vfo_config_compare(VFO_CONFIG_t*d1,VFO_CONFIG_t*d2);

//IF周波数，クロック源の周波数キャリブレーションデータを表す構造体
typedef struct{
	uint32_t IF;
	uint32_t crystal;
	uint16_t step;//キャリブレーション時のステップ周波数
}CALIBRATION_DATA_t;
uint8_t calibration_data_compare(CALIBRATION_DATA_t*d1,CALIBRATION_DATA_t*d2);

//電源の状態を表す構造体
typedef struct{
	uint16_t voltage;//電池電圧(mV)
	uint8_t is_ac_powered;//AC給電されているときnon zero
	uint8_t is_charging;//充電中はnon zero
}POWER_STATE_t;
uint8_t power_state_compare(POWER_STATE_t*d1,POWER_STATE_t*d2);

//送受信回路の動作状態
typedef struct{
	uint8_t is_transmitter_enable;
	uint8_t is_receiver_enable;

}RADIO_CIRCUIT_POWER_STATE_t;
uint8_t radio_circuit_power_state_compare(RADIO_CIRCUIT_POWER_STATE_t*d1,RADIO_CIRCUIT_POWER_STATE_t*d2);


//動作中のシステムの状態
typedef struct{
	VFO_CONFIG_t vfo_a;
	VFO_CONFIG_t vfo_b;
	uint8_t active_vfo;
	CALIBRATION_DATA_t calibration;
	POWER_STATE_t power_status;
	uint16_t rssi;
	RADIO_CIRCUIT_POWER_STATE_t radio_circuit_status;

}SYSTEM_STATUS_t;

#endif /* DATA_STRUCTURE_H_ */
