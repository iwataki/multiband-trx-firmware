/*
 * pinchange_wakeup.c
 *
 *  Created on: 2020/03/17
 *      Author: �@��Y
 */

#include "pinchange_wakeup.h"
#include <avr/io.h>
#include <avr/interrupt.h>

void pinchange_wakeup_enable(uint8_t ch){
	EICRB|=0x80;//int7 downedge
	EIMSK|=0x80;//int7 enable
	sei();
}
void pinchange_wakeup_disable(uint8_t ch){
	EIMSK&=~0x80;
	EICRB&=~0x80;
}
ISR(INT7_vect){

}

