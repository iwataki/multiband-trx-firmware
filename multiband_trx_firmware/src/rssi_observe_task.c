/*
 * rssi_observe_task.c
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */
#include "rssi_observe_task.h"
#include "hardware.h"

static SYSTEM_STATUS_t*state;
void rssi_observe_task_init(SYSTEM_STATUS_t*s){
	state=s;
}
void rssi_observe_task(void){
	uint16_t agc_mvolt=adc_get(&rssi_adc);
	int16_t rssi=(11*agc_mvolt-6000)/900;
	if(rssi<0){
		rssi=0;
	}
	state->rssi=(uint8_t)rssi;
}
