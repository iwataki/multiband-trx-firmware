/*
 * rssi_indicator.c
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */


#include "rssi_indicator.h"
#include "aqm1248.h"
#include "draw_system_status.h"

void rssi_indicator_create(RSSI_INDICATOR_t*r,uint8_t x,uint8_t y){
	r->x=x;
	r->y=y;
}
void rssi_indicator_setvalue(RSSI_INDICATOR_t*r,uint8_t rssi){
	print_text(r->x,r->y,2,"S");
	uint8_t graph_orgin_x=r->x+12;
	uint8_t graph_origin_y=r->y+8;
	print_text(graph_orgin_x,r->y,1," 1   5   9   +   ++");
	uint8_t tick_icon[2]={0x7e,0x7e};
	uint8_t doc_icon[2]={0x18,0x18};

	fill_rect(graph_orgin_x,graph_origin_y,graph_orgin_x+19*6,graph_origin_y+8,0);

	for(uint8_t i=0;i<19;i++){
		if(i%4==1){
			draw_icon(graph_orgin_x+i*6,graph_origin_y,1,tick_icon,2);
		}else{
			draw_icon(graph_orgin_x+i*6,graph_origin_y,1,doc_icon,2);
		}
	}
	uint8_t graph_tip;
	if(rssi>17){
		rssi=17;
	}
	graph_tip=graph_orgin_x+6*rssi+9;
	fill_rect(graph_orgin_x,graph_origin_y,graph_tip,graph_origin_y+8,1);
}
