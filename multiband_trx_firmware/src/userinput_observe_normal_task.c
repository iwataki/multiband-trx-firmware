/*
 * userinput_observe_normal_task.c
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */


#include "userinput_observe_normal_task.h"
#include "task.h"
#include "hardware.h"

static SYSTEM_STATUS_t*status;
static TASK_LIST_t*tasks;
static APPLICATION_TASK_ID_TABLE_t*task_id_table;
static uint8_t any_button_hold_counter;


void change_step(VFO_CONFIG_t*cfg){
	uint16_t step=cfg->step;
	if(step==1000){
		step=10000;
	}else{
		step=1000;
	}
	cfg->step=step;
}
void userinput_observe_normal_task_init(SYSTEM_STATUS_t*s,TASK_LIST_t*tasks_,APPLICATION_TASK_ID_TABLE_t*task_id_table_){
	status=s;
	tasks=tasks_;
	task_id_table=task_id_table_;
	any_button_hold_counter=0;
}
void userinput_observe_normal_task(){
	uint8_t left_button_event=get_switch_event(&left_button);
	uint8_t center_button_event=get_switch_event(&center_button);
	uint8_t right_button_event=get_switch_event(&right_button);
	if((left_button_event==USERSW_HOLD)||(center_button_event==USERSW_HOLD)||(right_button_event==USERSW_HOLD)){
		any_button_hold_counter++;
	}else{
		any_button_hold_counter=0;
	}
	if(any_button_hold_counter>200){//2sec hold
		any_button_hold_counter=0;
		switch_to_calibration_mode(tasks,task_id_table);
	}

	if(left_button_event==USERSW_PUSHED){
		if(status->active_vfo==0){
			status->active_vfo=1;
		}else{
			status->active_vfo=0;
		}
	}
	if(right_button_event==USERSW_PUSHED){
		if(status->active_vfo==0){
			change_step(&(status->vfo_a));
		}else{
			change_step(&(status->vfo_b));
		}
	}
	if(center_button_event==USERSW_PUSHED){
		uint8_t is_rit_enc_enable=get_task_status(tasks,task_id_table->normal_rit_encoder_task_id);
		uint8_t is_rit_enabled;
		RIT_CONFIG_t*rit_cfg;
		if(status->active_vfo==0){
			is_rit_enabled=status->vfo_a.rit_config.is_enable;
			rit_cfg=&(status->vfo_a.rit_config);
		}else{
			is_rit_enabled=status->vfo_b.rit_config.is_enable;
			rit_cfg=&(status->vfo_b.rit_config);
		}
		if(is_rit_enabled==0){
			rit_cfg->is_enable=1;
			activate_task(tasks,task_id_table->normal_rit_encoder_task_id);
			deactivate_task(tasks,task_id_table->normal_vfo_encoder_task_id);
		}else{
			if(is_rit_enc_enable==TASK_INACTIVE){
				rit_cfg->is_enable=0;
			}else{
				deactivate_task(tasks,task_id_table->normal_rit_encoder_task_id);
				activate_task(tasks,task_id_table->normal_vfo_encoder_task_id);
			}
		}
	}
}
