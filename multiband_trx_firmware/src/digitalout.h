/*
 * digitalout.h
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */

#ifndef DIGITALOUT_H_
#define DIGITALOUT_H_
#include <inttypes.h>

typedef struct {
	volatile uint8_t*DDR;
	volatile uint8_t*PORT;
	uint8_t bit;
}DIGITALOUT_t;

void init_digitalout(DIGITALOUT_t*d);
void digitalout_set(DIGITALOUT_t*d);
void digitalout_reset(DIGITALOUT_t*d);

#endif /* DIGITALOUT_H_ */
