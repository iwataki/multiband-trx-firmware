/*! \file i2ceeprom.c \brief Interface for standard I2C EEPROM memories. */
//*****************************************************************************
//
// File Name	: 'i2ceeprom.c'
// Title		: Interface for standard I2C EEPROM memories
// Author		: Pascal Stang - Copyright (C) 2003
// Created		: 2003.04.23
// Revised		: 2003.04.23
// Version		: 0.1
// Target MCU	: Atmel AVR series
// Editor Tabs	: 4
//
// This code is distributed under the GNU Public License
//		which can be found at http://www.gnu.org/licenses/gpl.txt
//
//*****************************************************************************

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/twi.h>

#include "i2c.h"

// Standard I2C bit rates are:
// 100KHz for slow speed
// 400KHz for high speed

// functions
void i2c_init(void)
{
	DDRC|=0x30;
	TWBR=0x20;
	TWSR&=0xfc;
}
char i2c_start(void){
	TWCR=SEND_START_COND;
	while((TWCR&_BV(TWINT))==0);
	if((TWSR&0xf8)==0x08){
		return 0;
	}else{
		return -1;
	}
}
void i2c_stop(void){
	TWCR=SEND_STOP_COND;
}

char i2c_send_addr(unsigned char devaddr){
	TWDR=devaddr;
	TWCR=I2C_TRANSACTION_START;
	while((TWCR&_BV(TWINT))==0);
	if((TWSR&0xf8)!=0x18){
		return -1;
	}
	return 0;
}
char i2c_send(int size,unsigned char*data){
	int i=0;
	char stat=0;
	for(i=0;i<size;i++){
		TWDR=(*data++);
		TWCR|=I2C_TRANSACTION_START;
		while((TWCR&_BV(TWINT))==0);
		if((TWSR&0xf8)!=0x28){
			stat=-1;
		}		
	}
	return stat;
}
char i2c_receive(int size,unsigned char*data){
	int i=0;
	for(i=0;i<(size-1);i++){
		TWCR|=_BV(TWEA)|I2C_TRANSACTION_START;
		while((TWCR&_BV(TWINT))==0);
		*(data++)=TWDR;
	}
	TWCR|=I2C_TRANSACTION_START;
	while((TWCR&_BV(TWINT))==0);
	*(data++)=TWDR;	
	return 0;	
}
void i2c_write(int addr,unsigned char*data,int len){
	i2c_start();
	i2c_send_addr(addr);
	i2c_send(len,data);
	i2c_stop();
}
