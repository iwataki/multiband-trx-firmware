/*
 * ptt_observe_calibration_task.c
 *
 *  Created on: 2020/03/20
 *      Author: �@��Y
 */

#include "ptt_observe_calibration_mode_task.h"

static SYSTEM_STATUS_t*status;
static USERSW_t*ptt;

void ptt_observe_calibration_mode_task_init(SYSTEM_STATUS_t*s,USERSW_t*ptt_){
	status=s;
	ptt=ptt_;
}

void ptt_observe_calibration_mode_task(void){
	uint8_t e=get_switch_event(ptt);
	if(e==USERSW_HOLD){
		status->radio_circuit_status.is_receiver_enable=1;
		status->radio_circuit_status.is_transmitter_enable=1;
	}
	if(e==USERSW_RELEASE){
		status->radio_circuit_status.is_receiver_enable=1;
		status->radio_circuit_status.is_transmitter_enable=0;
	}
}

void ptt_observe_calibration_mode_task_on_activated(void){
	status->radio_circuit_status.is_receiver_enable=1;
	status->radio_circuit_status.is_transmitter_enable=0;
}
