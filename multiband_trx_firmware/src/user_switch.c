/*
 * user_switch.c
 *
 *  Created on: 2016/11/01
 *      Author: �@��Y
 */
#include <avr/interrupt.h>
#include "user_switch.h"
	int prevport=0;
	int pin=0xff;
	int xoredpin=0;
void init_usersw(USERSW_t*sw){
	*(sw->DDR)&=~(1<<sw->bit);
	*(sw->PORT)|=(1<<sw->bit);
	sw->latest_state=*(sw->PIN)&(1<<sw->bit);
}
void update_switch(USERSW_t*sw){
	sw->previous_state=sw->latest_state;
	sw->latest_state=*(sw->PIN)&(1<<sw->bit);
	if(sw->latest_state==0){
		if(sw->previous_state==0){
			sw->latest_event=USERSW_HOLD;
		}else{
			sw->latest_event=USERSW_PUSHED;
		}
	}else{
		if(sw->previous_state==0){
			sw->latest_event=USERSW_RELEASED;
		}else{
			sw->latest_event=USERSW_RELEASE;
		}
	}
}
int get_switch(USERSW_t*sw){

	if(sw->latest_state==0){
		return 1;
	}else{
		return 0;
	}

}

int get_switch_event(USERSW_t*sw){
	int event=sw->latest_event;
	sw->latest_event=USERSW_NO_EVENT;
	return event;
}


