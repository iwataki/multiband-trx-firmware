/*
 * adc.h
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */

#ifndef ADC_H_
#define ADC_H_

#include <inttypes.h>

//reference voltage[mV]
#define ADC_VREF 3300
typedef struct{
	uint8_t ch;
}ADC_t;

void adc_init(ADC_t*adc,uint8_t ch);
uint16_t adc_get(ADC_t*adc);

#endif /* ADC_H_ */
