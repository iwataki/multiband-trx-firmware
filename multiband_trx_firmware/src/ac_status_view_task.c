/*
 * ac_status_view_task.c
 *
 *  Created on: 2020/03/18
 *      Author: 宗一郎
 */

#include "ac_status_view_task.h"
#include "system_constants.h"

static AC_STATUS_VIEW_t ac_status_view;
static POWER_STATE_t previous_state;
static POWER_STATE_t*power_state_;

uint8_t is_ac_status_changed(POWER_STATE_t*s1,POWER_STATE_t*s2){
	if(s1->is_ac_powered!=s2->is_ac_powered)return 1;
	if(s1->is_charging!=s2->is_charging)return 1;
	if(s1->voltage!=s2->voltage)return 1;
	return 0;
}
uint8_t voltage_to_battery_level(uint16_t mvolt){
	for(uint8_t i=0;i<5;i++){
		if(mvolt<battery_voltage_threshold[i]){
			return i;
		}
	}
	return 4;
}

void draw_ac_status(POWER_STATE_t*s){
	int volt=s->voltage/1000;//mv->volt
	int mvolt=(s->voltage%1000)/10;//小数点以下
	textbox_printf(&(ac_status_view.battery_voltage),"%1d.%02dV",volt,mvolt);
	battery_icon_setvalue(&(ac_status_view.battery_icon),voltage_to_battery_level(s->voltage));
	uint8_t status;
	if(s->is_charging){
		status=AC_ICON_CHRGING;
	}else if(s->is_ac_powered){
		status=AC_ICON_PLUGGED;
	}else{
		status=AC_ICON_BATTRY;
	}
	ac_icon_set_status(&(ac_status_view.ac_icon),status);
}

void ac_status_view_task_init(POWER_STATE_t*state){
	textbox_create(&(ac_status_view.battery_voltage),98,0,30,1);
	battery_icon_create(&(ac_status_view.battery_icon),90,0);
	ac_icon_create(&(ac_status_view.ac_icon),74,0);
	previous_state=*state;
	power_state_=state;
	draw_ac_status(power_state_);

}
void ac_status_view_task(void){
	if(power_state_compare(&previous_state,power_state_)){
		draw_ac_status(power_state_);
		previous_state=*power_state_;
	}

}
