/*
 * si5351.c
 *
 *  Created on: 2020/03/16
 *      Author: �@��Y
 */

#include <avr/io.h>
#include "si5351.h"
#include "i2c.h"
uint32_t si5351_range_clip(uint32_t val,uint32_t max,uint32_t min);
void si5351_clk_control(uint8_t ch,uint8_t state);
void si5351_write_register(uint8_t addr,uint8_t val){
	i2c_start();
	i2c_send_addr(SI5351_ADDR);
	i2c_send(1,&addr);
	i2c_send(1,&val);
	i2c_stop();

}
uint8_t si5351_read_register(uint8_t addr){
	int8_t result;
	result=i2c_start();
	result=i2c_send_addr(SI5351_ADDR);
	result=i2c_send(1,&addr);
	//i2c_stop();
	result=i2c_start();
	result=i2c_send_addr(SI5351_ADDR+1);
	uint8_t r;
	i2c_receive(1,&r);
	i2c_stop();
	return r;
}
uint8_t si5351_get_system_status(void){
	return si5351_read_register(0x00);
}

void init_si5351(void){
	i2c_init();
	while((si5351_get_system_status()&(1<<7))!=0);
	si5351_write_register(3,0xff);
	for(int i=0;i<8;i++){
		si5351_write_register(16+i,0x80);
	}
}

void si5351_set_multisynth(uint8_t id, uint32_t a, uint32_t b,
		uint32_t c) {
	uint32_t p1,p2,p3;
	p1=128*a+128*b/c-512;
	p2=128*b-c*(128*b/c);
	p3=c;
	uint8_t data[8]={(uint8_t)((p3>>8)&0xff),(uint8_t)(p3&0xff),
			(uint8_t)(((p1>>16)&0x03)),(uint8_t)((p1>>8)&0xff),(uint8_t)(p1&0xff),
			(uint8_t)(((p3>>12)&0xf0)|((p2>>16)&0x0f)),
			(uint8_t)((p2>>8)&0xff),(uint8_t)(p2&0xff)};
	uint8_t base_address=26+8*id;
	uint8_t i;
	for(i=0;i<8;i++){
		si5351_write_register(base_address+i,data[i]);
	}
}

void si5351_output_pin_config(uint8_t id, uint8_t power,uint8_t drive_level){
	uint8_t source;
	if(id==0){
		source=0;//ch0 source pll a
	}else{
		source=(1<<5);//ch1 source=pll b
	}
	unsigned char temp=(unsigned char)(power|source|0x40|0x0c|drive_level);//config output stage. power on/off,intmode,from_multisynth, drive level
	si5351_write_register(16+id,temp);
}

void si5351_reset_pll(uint8_t ch){
	unsigned char temp=si5351_read_register(177);
	temp|=(0x01<<(2*ch+5));//0xa0;
	si5351_write_register(177,temp);
}

void set_frequency(SI5351_CONFIG_t*d,uint8_t ch,uint32_t frequency){
	disable_clk(ch);
	si5351_output_pin_config(ch,SI5351_OUTPUT_POWER_OFF,0);
	uint32_t outputDivider=si5351_range_clip(PLL_MAX_FREQ/frequency,MULTISYNTH_MAX,MULTISYNTH_MIN);
	if(outputDivider%2!=0){
		outputDivider--;//outputdivider is even int
	}
	uint32_t pllfreq=outputDivider*frequency;
	uint32_t pll_ms_a,pll_ms_b,pll_ms_c=1048575;//pll multisynth value=a+b/c
	uint32_t xtal_freq_real=d->xtal_freq;
	pll_ms_a=pllfreq/xtal_freq_real;
	pll_ms_b=(uint32_t)((uint64_t)pll_ms_c*(uint64_t)(pllfreq%xtal_freq_real)/xtal_freq_real);
	si5351_set_multisynth(ch,pll_ms_a,pll_ms_b,pll_ms_c);//pll config
	si5351_set_multisynth(2+ch,outputDivider,0,pll_ms_c);//output multisynth config
	si5351_output_pin_config(ch,SI5351_OUTPUT_POWER_ON,3);
	si5351_write_register(149,0);//disable ss
	si5351_write_register(187,0xc8);
	si5351_reset_pll(ch);
	enable_clk(ch);

}
void enable_clk(uint8_t ch){
	si5351_clk_control(ch,1);
}
void disable_clk(uint8_t ch){
	si5351_clk_control(ch,0);
}

uint32_t si5351_range_clip(uint32_t val,uint32_t max,uint32_t min){
	if(val>max){
		return max;
	}
	if(val<min){
		return min;
	}
	return val;
}
void si5351_clk_control(uint8_t ch,uint8_t state){
	unsigned char temp=si5351_read_register(0x03);
	temp&=~(1<<ch);
	if(state==0){
		temp|=(1<<ch);
	}
	si5351_write_register(0x03,temp);
}
