/*
 * normal_encoder_task.h
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */

#ifndef NORMAL_VFO_ENCODER_TASK_H_
#define NORMAL_VFO_ENCODER_TASK_H_

#include "data_structure.h"

void normal_vfo_encoder_task_init(SYSTEM_STATUS_t*status);
void normal_vfo_encoder_task(void);

#endif /* NORMAL_VFO_ENCODER_TASK_H_ */
