/*
 * battery_management_task.h
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */

#ifndef BATTERY_MANAGEMENT_TASK_H_
#define BATTERY_MANAGEMENT_TASK_H_
#include "data_structure.h"

void battery_management_task_init(POWER_STATE_t*power_state);
void battery_management_task(void);


#endif /* BATTERY_MANAGEMENT_TASK_H_ */
