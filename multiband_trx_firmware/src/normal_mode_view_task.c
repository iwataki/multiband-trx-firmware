/*
 * draw_system_status_task.c
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */
#include "normal_mode_view_task.h"
#include "aqm1248.h"

static NORMAL_MODE_VIEW_t normal_mode_view;
static NORMAL_MODE_VIEW_DATA_t prev_view_data;
static SYSTEM_STATUS_t*state_;

void normal_mode_view_copy_to_view_data(SYSTEM_STATUS_t*origin,NORMAL_MODE_VIEW_DATA_t*data){
	data->active_vfo=origin->active_vfo;
	data->rssi=origin->rssi;
	if(data->active_vfo==0){
		data->vfo_config=origin->vfo_a;
	}else{
		data->vfo_config=origin->vfo_b;
	}
}

uint8_t is_normal_mode_changed(NORMAL_MODE_VIEW_DATA_t*d1,NORMAL_MODE_VIEW_DATA_t*d2){
	if(d1->active_vfo!=d2->active_vfo)return 1;
	if(d1->rssi!=d2->rssi)return 1;
	if(vfo_config_compare(&(d1->vfo_config),&(d2->vfo_config))!=0)return 1;
	return 0;
}
void draw_normal_mode_view(NORMAL_MODE_VIEW_t*v,NORMAL_MODE_VIEW_DATA_t*d){
	if(d->active_vfo==0){
		textbox_printf(&(v->active_vfo),"VFO A");
	}else{
		textbox_printf(&(v->active_vfo),"VFO B");
	}
	uint16_t MHz=d->vfo_config.frequency/1000000;
	uint32_t Hz=d->vfo_config.frequency%1000000;
	uint16_t kHz=Hz/1000;
	textbox_printf(&(v->frequency),"%2d.%03dMHz",MHz,kHz);
	if(d->vfo_config.rit_config.is_enable!=0){
		textbox_printf(&(v->rit),"R%-+4d",d->vfo_config.rit_config.frequency);
	}else{
		textbox_printf(&(v->rit),"       ");
	}
	rssi_indicator_setvalue(&(v->rssi),d->rssi);
}

void normal_mode_view_init(SYSTEM_STATUS_t*state){
	textbox_create(&(normal_mode_view.active_vfo),0,0,30,1);
	textbox_create(&(normal_mode_view.frequency),0,8,108,2);
	textbox_create(&(normal_mode_view.rit),30,0,44,1);
	rssi_indicator_create(&(normal_mode_view.rssi),0,24);

	state_=state;
	prev_view_data.active_vfo=state->active_vfo;
	prev_view_data.rssi=state->rssi;
	normal_mode_view_copy_to_view_data(state,&prev_view_data);

}

void normal_mode_view_on_activated(){
	fill_rect(0,8,128,48,0);//erase main window
	draw_normal_mode_view(&normal_mode_view,&prev_view_data);
}

void normal_mode_view_task(void){
	NORMAL_MODE_VIEW_DATA_t d;
	normal_mode_view_copy_to_view_data(state_,&d);
	if(is_normal_mode_changed(&d,&prev_view_data)!=0){
		draw_normal_mode_view(&normal_mode_view,&d);
		prev_view_data=d;
	}
}
