/*
 * system_state_store.c
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */

#include "system_state_store.h"
#include "system_constants.h"

SYSTEM_STATUS_t system_status;

//set system state from config
void init_system_status(SystemNonvolatileConfig_t*config){
	if(config!=0){
		system_status.active_vfo=0;
		system_status.calibration=config->calibration;
		system_status.rssi=0;
		system_status.power_status.is_ac_powered=0;
		system_status.power_status.is_charging=0;
		system_status.power_status.voltage=0;
		system_status.vfo_a=config->vfo_a;
		system_status.vfo_b=config->vfo_b;
	}else{//load default value
		system_status.active_vfo=0;
		system_status.calibration.IF=DEFAULT_IF_FREQ;
		system_status.calibration.crystal=DEFAULT_XTAL_FREQ;
		system_status.calibration.step=100;
		system_status.radio_circuit_status.is_receiver_enable=0;
		system_status.radio_circuit_status.is_transmitter_enable=0;
		system_status.vfo_a.frequency=DEFAULT_VFO_FREQ;
		system_status.vfo_a.step=1000;
		system_status.vfo_a.rit_config.frequency=0;
		system_status.vfo_a.rit_config.is_enable=0;
		system_status.vfo_b=system_status.vfo_a;
		system_status.rssi=0;
	}
}
//backup system state to config
void copy_system_status(SystemNonvolatileConfig_t*config){
	if(config!=0){
		config->calibration=system_status.calibration;
		config->vfo_a=system_status.vfo_a;
		config->vfo_b=system_status.vfo_b;
	}
}
