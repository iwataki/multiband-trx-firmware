/*
 * aqm1248.c
 *
 *  Created on: 2020/03/16
 *      Author: 宗一郎
 */


#include <avr/io.h>
#include <util/delay.h>
#include "aqm1248.h"
#include "font.h"


void aqm1248_send(uint8_t command){
	SPDR=command;
	while((SPSR&0x80)==0);//wait spi tranfer complete
}


void aqm1248_cs_assert(void){
	AQM1248_PORT&=~(1<<AQM1248_CS);
}
void aqm1248_cs_negate(void){
	AQM1248_PORT|=(1<<AQM1248_CS);
}

void aqm1248_rs_command(void){
	AQM1248_PORT&=~(1<<AQM1248_RS);
}
void aqm1248_rs_data(void){
	AQM1248_PORT|=(1<<AQM1248_RS);
}

void init_aqm1248(void){
	//port init
	AQM1248_DDR|=((1<<AQM1248_CS)|(1<<AQM1248_RS)|(1<<3)|(1<<5));//config CS,RS,MOSI,SCK output

	//spi init
	SPSR|=(1<<0);//set spi2x
	SPCR|=((1<<6)|(1<<4)|(1<<3)|(1<<2));//spi enable, master mode, cpol=1, cpha=1, sck=1/2 fosc
	aqm1248_cs_negate();
	//send init command
	aqm1248_rs_command();
	aqm1248_cs_assert();

	aqm1248_send(0xae);
	aqm1248_send(0xa0);
	aqm1248_send(0xc8);
	aqm1248_send(0xa3);

	aqm1248_send(0x2c);
	_delay_ms(2);
	aqm1248_send(0x2e);
	_delay_ms(2);
	aqm1248_send(0x2f);

	aqm1248_send(0x23);
	aqm1248_send(0x81);
	aqm1248_send(0x1c);

	aqm1248_send(0xa4);
	aqm1248_send(0x40);
	aqm1248_send(0xa6);
	aqm1248_send(0xaf);

	aqm1248_cs_negate();

}
void draw_hline(uint8_t x1,uint8_t y1,uint8_t x2,uint8_t color){
	uint8_t pixel_data=1<<(y1%8);
	if(color){
		pixel_data=0;
	}
	uint8_t page=y1/8;
	if(x1>x2){
		swap(&x1,&x2);
	}
	uint8_t column_start=x1;
	aqm1248_rs_command();
	aqm1248_cs_assert();
	aqm1248_send(AQM1248_PAGE_ADDRESS_SET|page);
	aqm1248_send(AQM1248_COLUMN_ADDRSS_SET_UPPER|((column_start>>4)&0x0f));
	aqm1248_send(AQM1248_COLUMN_ADDRSS_SET_LOWER|(column_start&0x0f));
	aqm1248_rs_data();
	for(uint8_t i=0;i<x2-x1;i++){
		aqm1248_send(pixel_data);
	}
	aqm1248_cs_negate();
}
void draw_vline(uint8_t x1,uint8_t y1,uint8_t y2,uint8_t color){
	uint8_t pixel_data=0xff;
	if(color){
		pixel_data=0;
	}
	if(y1>y2){
		swap(&y1,&y2);
	}
	uint8_t page_start=y1/8;
	uint8_t page_end=y2/8;
	uint8_t column=x1;

	aqm1248_rs_command();
	aqm1248_cs_assert();
	aqm1248_send(AQM1248_COLUMN_ADDRSS_SET_UPPER|((column>>4)&0x0f));
	aqm1248_send(AQM1248_COLUMN_ADDRSS_SET_LOWER|(column&0x0f));
	for(uint8_t i=0;i<page_end-page_start;i++){
		aqm1248_rs_command();
		aqm1248_send(AQM1248_PAGE_ADDRESS_SET|(i+page_start));
		aqm1248_rs_data();
		aqm1248_send(pixel_data);
	}
	aqm1248_cs_negate();
}
//塗りつぶし四角形を描画
void fill_rect(uint8_t x1,uint8_t y1,uint8_t x2,uint8_t y2,uint8_t color){
	uint8_t pixeldata=0xff;
	if(color==0){
		pixeldata=0;
	}
	if(x1>x2){
		swap(&x1,&x2);
	}
	if(y1>y2){
		swap(&y1,&y2);
	}
	uint8_t page_start=y1/8;
	uint8_t page_end=y2/8;
	aqm1248_cs_assert();
	for(uint8_t i=page_start;i<page_end;i++){
		aqm1248_rs_command();
		aqm1248_send(AQM1248_PAGE_ADDRESS_SET|i);
		aqm1248_send(AQM1248_COLUMN_ADDRSS_SET_UPPER|((x1>>4)&0x0f));
		aqm1248_send(AQM1248_COLUMN_ADDRSS_SET_LOWER|(x1&0x0f));
		aqm1248_rs_data();
		for(uint8_t j=x1;j<x2;j++){
			aqm1248_send(pixeldata);
		}
	}
	aqm1248_cs_negate();

}
//文字を描画
void draw_char(uint8_t x1,uint8_t y1,uint8_t size,uint8_t c){
	uint8_t page_start=y1/8;
	aqm1248_cs_assert();
	for(uint8_t i=0;i<size;i++){
		aqm1248_rs_command();
		aqm1248_send(AQM1248_PAGE_ADDRESS_SET|(page_start+i));
		aqm1248_send(AQM1248_COLUMN_ADDRSS_SET_UPPER|((x1>>4)&0x0f));
		aqm1248_send(AQM1248_COLUMN_ADDRSS_SET_LOWER|(x1&0x0f));
		aqm1248_rs_data();
		for(uint8_t j=0;j<5;j++){
			uint8_t font_slice=pgm_read_byte(font+c*5+j);
			uint8_t pixeldata=0;
			for(uint8_t bit=0;bit<8;bit++){
				uint8_t pixel_v_position=8*i+bit;
				uint8_t font_v_pos=pixel_v_position/size;
				if((font_slice&(1<<font_v_pos))!=0){
					pixeldata|=(1<<bit);
				}
			}
			for(uint8_t k=0;k<size;k++){
				aqm1248_send(pixeldata);
			}
		}
		for(uint8_t j=0;j<size;j++){
			aqm1248_send(0);//space between char
		}
	}
	aqm1248_cs_negate();

}
//dataで示されるビットマップのアイコンを描画
void draw_icon(uint8_t x1,uint8_t y1,uint8_t size,uint8_t*data,uint8_t len){
	uint8_t page_start=y1/8;
	uint8_t width=len/size;
	aqm1248_cs_assert();
	for(uint8_t i=0;i<size;i++){
		aqm1248_rs_command();
		aqm1248_send(AQM1248_PAGE_ADDRESS_SET|(page_start+i));
		aqm1248_send(AQM1248_COLUMN_ADDRSS_SET_UPPER|((x1>>4)&0x0f));
		aqm1248_send(AQM1248_COLUMN_ADDRSS_SET_LOWER|(x1&0x0f));
		aqm1248_rs_data();
		for(uint8_t j=0;j<width;j++){
			aqm1248_send(data[i*width+j]);
		}
	}
	aqm1248_cs_negate();
}

void clear_screen(void){
	aqm1248_cs_assert();
	for(uint8_t i=0;i<AQM1248_PAGE_SIZE;i++){
		aqm1248_rs_command();
		aqm1248_send(AQM1248_PAGE_ADDRESS_SET|i);
		aqm1248_send(AQM1248_COLUMN_ADDRSS_SET_UPPER);
		aqm1248_send(AQM1248_COLUMN_ADDRSS_SET_LOWER);
		aqm1248_rs_data();
		for(uint8_t j=0;j<AQM1248_COLUMN_SIZE;j++){
			aqm1248_send(0);
		}
	}
	aqm1248_cs_negate();
}

void aqm1248_sleep(void){
	aqm1248_cs_assert();
	aqm1248_rs_command();
	aqm1248_send(AQM1248_DISPLAY_ONOFF|0);//off
	aqm1248_send(AQM1248_DISPLAY_ALL_POINTS|1);//display all points on
	aqm1248_cs_negate();

}
void aqm1248_wakeup(void){
	aqm1248_cs_assert();
	aqm1248_rs_command();
	aqm1248_send(AQM1248_DISPLAY_ALL_POINTS|0);//display all points off
	aqm1248_send(AQM1248_DISPLAY_ONOFF|1);//on
	aqm1248_cs_negate();
}

void swap(uint8_t*x,uint8_t*y){
	uint8_t temp=*x;
	*x=*y;
	*y=temp;
}
