/*
 * power_management_task.h
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */

#ifndef POWER_MANAGEMENT_TASK_H_
#define POWER_MANAGEMENT_TASK_H_
#include "user_switch.h"
#include "data_structure.h"

void init_power_management_task(SYSTEM_STATUS_t*s,USERSW_t*power_sw);
void power_management_task(void);

#endif /* POWER_MANAGEMENT_TASK_H_ */
