/*
 * pinchange_wakeup.h
 *
 *  Created on: 2020/03/17
 *      Author: �@��Y
 */

#ifndef PINCHANGE_WAKEUP_H_
#define PINCHANGE_WAKEUP_H_

#include <inttypes.h>
void pinchange_wakeup_enable(uint8_t ch);
void pinchange_wakeup_disable(uint8_t ch);

#endif /* PINCHANGE_WAKEUP_H_ */
