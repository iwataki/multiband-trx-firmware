/*
 * calibration_view_task.c
 *
 *  Created on: 2020/03/20
 *      Author: �@��Y
 */

#include "calibration_view_task.h"
#include "calibration_target.h"
#include "aqm1248.h"

static SYSTEM_STATUS_t*status;
static CALIBRATION_MODE_VIEW_t view;
static CALIBRATION_MODE_VIEW_DATA_t previous_data;
static TASK_LIST_t*tasks;
static APPLICATION_TASK_ID_TABLE_t*task_ids;

void calibration_view_copy_data(SYSTEM_STATUS_t*s,TASK_LIST_t*t,APPLICATION_TASK_ID_TABLE_t*ids,CALIBRATION_MODE_VIEW_DATA_t*data){
	data->calibration_target=get_cal_target(t,ids);
	data->calibration_data=s->calibration;
	data->rssi=s->rssi;
}
uint8_t calibration_view_data_compare(CALIBRATION_MODE_VIEW_DATA_t*d1,CALIBRATION_MODE_VIEW_DATA_t*d2){
	if(calibration_data_compare(&(d1->calibration_data),&(d2->calibration_data))!=0)return 1;
	if(d1->calibration_target!=d2->calibration_target)return 1;
	if(d1->rssi!=d2->rssi)return 1;
	return 0;
}

void calibration_view_data_draw(CALIBRATION_MODE_VIEW_DATA_t*d,CALIBRATION_MODE_VIEW_t*v){
	uint16_t f_MHz;
	uint32_t f_Hz;
	uint32_t f;
	if(d->calibration_target==CAL_TARGET_XTAL){
		textbox_printf(&(v->calibration_target),"XTAL");
		f=d->calibration_data.crystal;
	}else{
		textbox_printf(&(v->calibration_target),"IF  ");
		f=d->calibration_data.IF;

	}
	f_MHz=f/1000000;
	f_Hz=f%1000000;
	textbox_printf(&(v->frequency),"%02d.%06ldMHz",f_MHz,f_Hz);
	rssi_indicator_setvalue(&(v->rssi),d->rssi);
}
void calibration_view_task_on_activated(void){
	fill_rect(0,8,128,48,0);//erase main window
	calibration_view_data_draw(&previous_data,&view);
}
void calibration_view_task_init(SYSTEM_STATUS_t*s,TASK_LIST_t*t,APPLICATION_TASK_ID_TABLE_t*ids){
	status=s;
	tasks=t;
	task_ids=ids;

	calibration_view_copy_data(status,t,ids,&previous_data);

	textbox_create(&(view.calibration_target),0,8,24,1);
	textbox_create(&(view.frequency),0,16,128,1);
	rssi_indicator_create(&(view.rssi),0,24);
}
void calibration_view_task(void){
	CALIBRATION_MODE_VIEW_DATA_t data;
	calibration_view_copy_data(status,tasks,task_ids,&data);
	if(calibration_view_data_compare(&data,&previous_data)!=0){
		calibration_view_data_draw(&data,&view);
		previous_data=data;
	}

}
