/*
 * normal_vfo_output_task.h
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */

#ifndef VFO_OUTPUT_TASK_H_
#define VFO_OUTPUT_TASK_H_

#include "data_structure.h"

void vfo_output_task_init(SYSTEM_STATUS_t*s);
void vfo_output_task();

#endif /* VFO_OUTPUT_TASK_H_ */
