/*
 * normal_vfo_encoder_task.c
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */

#include "normal_vfo_encoder_task.h"
#include "hardware.h"
#include "system_constants.h"

static SYSTEM_STATUS_t*status;
static int diff_incr=0;
void normal_vfo_encoder_task_init(SYSTEM_STATUS_t*status_){
	status=status_;
}
void normal_vfo_encoder_task(void){
	diff_incr+=get_encoder_diff(&encoder);
	int diff_half=diff_incr/2;
	if(diff_half!=0){
		diff_incr=0;
	}
	VFO_CONFIG_t*vfo_config;
	if(status->active_vfo==0){
		vfo_config=&(status->vfo_a);
	}else{
		vfo_config=&(status->vfo_b);
	}

	int freq_change=vfo_config->step*diff_half;
	vfo_config->frequency+=freq_change;
	if(vfo_config->frequency>VFO_FREQ_MAX){
		vfo_config->frequency=VFO_FREQ_MAX;
	}
	if(vfo_config->frequency<VFO_FREQ_MIN){
		vfo_config->frequency=VFO_FREQ_MIN;
	}
}
