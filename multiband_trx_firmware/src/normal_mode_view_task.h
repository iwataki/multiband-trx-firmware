/*
 * draw_system_status_task.h
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */

#ifndef NORMAL_MODE_VIEW_TASK_H_
#define NORMAL_MODE_VIEW_TASK_H_
#include "data_structure.h"
#include "textbox.h"
#include "rssi_indicator.h"

typedef struct{
	TEXTBOX_t active_vfo;
	TEXTBOX_t frequency;
	TEXTBOX_t rit;
	RSSI_INDICATOR_t rssi;
}NORMAL_MODE_VIEW_t;

typedef struct{
	VFO_CONFIG_t vfo_config;
	uint8_t active_vfo;
	uint16_t rssi;
}NORMAL_MODE_VIEW_DATA_t;

void normal_mode_view_init(SYSTEM_STATUS_t*state);
void normal_mode_view_on_activated();
void normal_mode_view_task(void);

#endif /* NORMAL_MODE_VIEW_TASK_H_ */
