/*
 * vfo_control.c
 *
 *  Created on: 2020/03/22
 *      Author: �@��Y
 */
#include "vfo_control.h"
#include "hardware.h"
#include "system_constants.h"
#include "si5351.h"

void apply_vfo_state(VFO_CONFIG_t*vfo,RADIO_CIRCUIT_POWER_STATE_t*radio,CALIBRATION_DATA_t*cal){
	SI5351_CONFIG_t cfg;
	cfg.xtal_freq=cal->crystal;
	if(radio->is_receiver_enable){
		uint32_t local_freq=vfo->frequency-cal->IF;
		if(vfo->rit_config.is_enable!=0){
			local_freq+=vfo->rit_config.frequency;
		}
		set_frequency(&cfg,VFO_RX_CHANNEL,local_freq);
		enable_clk(VFO_RX_CHANNEL);
	}else{
		disable_clk(VFO_RX_CHANNEL);
	}
	if(radio->is_transmitter_enable){
		set_frequency(&cfg,VFO_TX_CHANNEL,vfo->frequency);
		enable_clk(VFO_TX_CHANNEL);
	}else{
		disable_clk(VFO_TX_CHANNEL);
	}
}
