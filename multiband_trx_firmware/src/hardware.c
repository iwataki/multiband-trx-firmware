/*
 * hardware.c
 *
 *  Created on: 2020/03/17
 *      Author: �@��Y
 */

#include "hardware.h"
#include "aqm1248.h"
#include "si5351.h"


USERSW_t left_button;
USERSW_t center_button;
USERSW_t right_button;
ENCODER_t encoder;
USERSW_t volume_sw;
USERSW_t ptt;
ADC_t battery_level_adc;
ADC_t rssi_adc;
ADC_t battery_charge_adc;
DIGITALOUT_t battery_chaege_enable_pin;
DIGITALOUT_t acinput_select_pin;
USERSW_t ac_presense_input;
USERSW_t power_alarm_input;
DIGITALOUT_t transmitter_enable_pin;
DIGITALOUT_t receiver_enable_pin;

void init_hardware(void){
	left_button.DDR=&DDRE;
	left_button.PORT=&PORTE;
	left_button.PIN=&PINE;
	left_button.bit=7;
	init_usersw(&left_button);

	center_button=left_button;
	center_button.bit=6;
	init_usersw(&center_button);

	right_button=left_button;
	right_button.bit=5;
	init_usersw(&right_button);

	encoder.DDR_phaseA=&DDRE;
	encoder.DDR_phaseB=&DDRE;
	encoder.PIN_phaseA=&PINE;
	encoder.PIN_phaseB=&PINE;
	encoder.PORT_phaseA=&PORTE;
	encoder.PORT_phaseB=&PORTE;
	encoder.bitA=2;
	encoder.bitB=3;
	init_enc(&encoder,-1);

	volume_sw=left_button;
	volume_sw.bit=4;
	init_usersw(&volume_sw);

	ptt.DDR=&DDRA;
	ptt.PIN=&PINA;
	ptt.PORT=&PORTA;
	ptt.bit=7;
	init_usersw(&ptt);

	init_aqm1248();
	init_si5351();

	adc_init(&battery_charge_adc,3);
	adc_init(&battery_level_adc,6);
	adc_init(&rssi_adc,7);

	ac_presense_input.DDR=&DDRD;
	ac_presense_input.PIN=&PIND;
	ac_presense_input.PORT=&PORTD;
	ac_presense_input.bit=2;
	power_alarm_input.DDR=&DDRB;
	power_alarm_input.PIN=&PINB;
	power_alarm_input.PORT=&PORTB;
	power_alarm_input.bit=7;
	init_usersw(&ac_presense_input);
	init_usersw(&power_alarm_input);

	battery_chaege_enable_pin.DDR=&DDRB;
	battery_chaege_enable_pin.PORT=&PORTB;
	battery_chaege_enable_pin.bit=6;
	init_digitalout(&battery_chaege_enable_pin);

	acinput_select_pin=battery_chaege_enable_pin;
	acinput_select_pin.bit=0;
	init_digitalout(&acinput_select_pin);

	transmitter_enable_pin.DDR=&DDRC;
	transmitter_enable_pin.PORT=&PORTC;
	transmitter_enable_pin.bit=1;
	init_digitalout(&transmitter_enable_pin);

	receiver_enable_pin=transmitter_enable_pin;
	receiver_enable_pin.bit=2;
	init_digitalout(&receiver_enable_pin);

}

void update_hardware(void){
	update_switch(&center_button);
	update_switch(&left_button);
	update_switch(&right_button);
	update_switch(&ptt);
	update_switch(&volume_sw);
	update_encoder(&encoder);
	update_switch(&ac_presense_input);
	update_switch(&power_alarm_input);

}
