/*
 * aqm1248.h
 *
 *  Created on: 2020/03/16
 *      Author: 宗一郎
 */

#ifndef AQM1248_H_
#define AQM1248_H_

#include <inttypes.h>

#define AQM1248_COLUMN_SIZE 128
#define AQM1248_PAGE_SIZE 6

#define AQM1248_DDR DDRB
#define AQM1248_PORT PORTB
#define AQM1248_CS 2
#define AQM1248_RS 1

//command set
#define AQM1248_DISPLAY_ONOFF 0xae
#define AQM1248_DISPLAY_STARTLINE_SET 0x40
#define AQM1248_PAGE_ADDRESS_SET 0xb0
#define AQM1248_COLUMN_ADDRSS_SET_UPPER 0x10
#define AQM1248_COLUMN_ADDRSS_SET_LOWER 0x00
#define AQM1248_ADC_SELECT 0xa0
#define AQM1248_DISPLAY_NORMAL_REVERSE 0xa6
#define AQM1248_LCD_BIAS_SET 0xa2
#define AQM1248_READ_MODIFY_WRITE 0x70
#define AQM1248_RESET 0x72
#define AQM1248_COMMON_OUTPUT_MODE 0xc0
#define AQM1248_POWER_CONTROL_SET 0x28
#define AQM1248_REG_RATIO_SET 0x20
#define AQM1248_VOLUME_SET 0x81
#define AQM1248_SLEEPMODE_SET 0xac
#define AQM1248_DISPLAY_ALL_POINTS 0xa4

void init_aqm1248(void);
//水平線分を描画
void draw_hline(uint8_t x1,uint8_t y1,uint8_t x2,uint8_t color);
//垂直線分を描画　y1は8の倍数
void draw_vline(uint8_t x1,uint8_t y1,uint8_t y2,uint8_t color);
//塗りつぶし四角形を描画　y1は8の倍数
void fill_rect(uint8_t x1,uint8_t y1,uint8_t x2,uint8_t y2,uint8_t color);
//文字を描画　y1は8の倍数 size=1で縦8ドット 2で16ドット
void draw_char(uint8_t x1,uint8_t y1,uint8_t size,uint8_t c);
//dataで示されるビットマップのアイコンを描画(縦8*size dot,横任意)　y1は8の倍数
void draw_icon(uint8_t x1,uint8_t y1,uint8_t size,uint8_t*data,uint8_t len);

//画面消去
void clear_screen(void);

void aqm1248_sleep(void);
void aqm1248_wakeup(void);

void swap(uint8_t*x,uint8_t*y);

#endif /* AQM1248_H_ */
