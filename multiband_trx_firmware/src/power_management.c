/*
 * power_management.c
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */

#include <avr/io.h>
#include <avr/sleep.h>
#include "pinchange_wakeup.h"
#include "power_management.h"

void system_poweron(void){
	DDRC|=(1<<0);
	PORTC|=(1<<0);

}
void system_poweroff(void){
	PORTC&=~(1<<0);

}

void system_sleep(void){
	pinchange_wakeup_enable(20);
	pinchange_wakeup_enable(18);
	set_sleep_mode(SLEEP_MODE_PWR_DOWN);
	sleep_enable();
	sleep_cpu();
	sleep_disable();
	pinchange_wakeup_disable(20);
	pinchange_wakeup_disable(18);
}


void system_idle(void){
	set_sleep_mode(SLEEP_MODE_IDLE);
	sleep_enable();
	sleep_cpu();
	sleep_disable();
}
