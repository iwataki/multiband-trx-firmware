/*
 * userinput_observe_normal_task.h
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */

#ifndef USERINPUT_OBSERVE_NORMAL_TASK_H_
#define USERINPUT_OBSERVE_NORMAL_TASK_H_
#include "application.h"
#include "data_structure.h"

void userinput_observe_normal_task_init(SYSTEM_STATUS_t*s,TASK_LIST_t*tasks,APPLICATION_TASK_ID_TABLE_t*task_id_table);
void userinput_observe_normal_task();


#endif /* USERINPUT_OBSERVE_NORMAL_TASK_H_ */
