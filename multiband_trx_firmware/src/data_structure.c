/*
 * data_structure.c
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */

#include "data_structure.h"

uint8_t rit_config_compare(RIT_CONFIG_t*d1,RIT_CONFIG_t*d2){
	if(d1->frequency!=d2->frequency)return 1;
	if(d1->is_enable!=d2->is_enable)return 1;
	return 0;
}

uint8_t vfo_config_compare(VFO_CONFIG_t*d1,VFO_CONFIG_t*d2){
	if(d1->frequency!=d2->frequency)return 1;
	if(rit_config_compare(&(d1->rit_config),&(d2->rit_config))!=0)return 1;
	if(d1->step!=d2->step)return 1;
	return 0;
}

uint8_t calibration_data_compare(CALIBRATION_DATA_t*d1,CALIBRATION_DATA_t*d2){
	if(d1->IF!=d2->IF)return 1;
	if(d1->crystal!=d2->crystal)return 1;
	return 0;
}

uint8_t power_state_compare(POWER_STATE_t*d1,POWER_STATE_t*d2){
	if(d1->is_ac_powered!=d2->is_ac_powered)return 1;
	if(d1->is_charging!=d2->is_charging)return 1;
	if(d1->voltage!=d2->voltage)return 1;
	return 0;
}

uint8_t radio_circuit_power_state_compare(RADIO_CIRCUIT_POWER_STATE_t*d1,RADIO_CIRCUIT_POWER_STATE_t*d2){
	if(d1->is_receiver_enable!=d2->is_receiver_enable)return 1;
	if(d1->is_transmitter_enable!=d2->is_transmitter_enable)return 1;
	return 0;
}
