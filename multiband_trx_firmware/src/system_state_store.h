/*
 * system_state_store.h
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */

#ifndef SYSTEM_STATE_STORE_H_
#define SYSTEM_STATE_STORE_H_

#include "data_structure.h"
#include "config_memory.h"

extern SYSTEM_STATUS_t system_status;

//set system state from config
void init_system_status(SystemNonvolatileConfig_t*config);
//backup system state to config
void copy_system_status(SystemNonvolatileConfig_t*config);

#endif /* SYSTEM_STATE_STORE_H_ */
