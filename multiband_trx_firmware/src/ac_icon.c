/*
 * ac_icon.c
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */
#include "ac_icon.h"
#include "aqm1248.h"
#include <avr/pgmspace.h>

const uint8_t spark_icon_data[]PROGMEM={
	0x00,0x10,0x1c,0xff,0x37,0x13,0x03,0x01
};

const uint8_t plug_icon_data[]PROGMEM={
	0x00,0x3c,0x7f,0x7c,0x7c,0x7f,0x3c,0x00
};


void ac_icon_create(AC_ICON_t*icon,uint8_t x,uint8_t y){
	icon->x=x;
	icon->y=y;
}

void ac_icon_set_status(AC_ICON_t*icon,uint8_t status){
	uint8_t icon_data[8];
	if((status==AC_ICON_CHRGING)||(status==AC_ICON_PLUGGED)){
		for(uint8_t i=0;i<8;i++){
			icon_data[i]=pgm_read_byte(plug_icon_data+i);
		}
	}else{
		for(uint8_t i=0;i<8;i++){
			icon_data[i]=0;
		}
	}
	draw_icon(icon->x,icon->y,1,icon_data,8);


	if(status==AC_ICON_CHRGING){
		for(uint8_t i=0;i<8;i++){
			icon_data[i]=pgm_read_byte(spark_icon_data+i);
		}
	}else{
		for(uint8_t i=0;i<8;i++){
			icon_data[i]=0;
		}
	}
	draw_icon(icon->x+8,icon->y,1,icon_data,8);
}
