/*
 * ac_icon.h
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */

#ifndef AC_ICON_H_
#define AC_ICON_H_

#include <inttypes.h>
#define AC_ICON_CHRGING 0
#define AC_ICON_PLUGGED 1
#define AC_ICON_BATTRY 2

typedef struct{
	uint8_t x;
	uint8_t y;
}AC_ICON_t;

void ac_icon_create(AC_ICON_t*icon,uint8_t x,uint8_t y);
void ac_icon_set_status(AC_ICON_t*icon,uint8_t status);

#endif /* AC_ICON_H_ */
