/*
 * application.c
 *
 *  Created on: 2020/03/18
 *      Author: 宗一郎
 */
#include "application.h"
#include "task.h"
#include "power_management_task.h"
#include "hardware.h"
#include "system_state_store.h"
#include "config_memory.h"
#include "normal_mode_view_task.h"
#include "ac_status_view_task.h"
#include "battery_management_task.h"
#include "normal_vfo_encoder_task.h"
#include "vfo_output_task.h"
#include "radio_circuit_control_task.h"
#include "ptt_observe_normal_task.h"
#include "rssi_observe_task.h"
#include "userinput_observe_normal_task.h"
#include "userinput_observe_calibration_task.h"
#include "calibration_view_task.h"
#include "xtal_calibration_encoder_task.h"
#include "if_calibration_encoder_task.h"
#include "normal_rit_encoder_task.h"
#include "ptt_observe_calibration_mode_task.h"

TASK_LIST_t application_task_list;
APPLICATION_TASK_ID_TABLE_t task_id_table;

void init_application(void){
	init_task(&application_task_list);

	SystemNonvolatileConfig_t cfg;
	uint8_t config_load_result=load_config(&cfg);
	if(config_load_result==0){//不揮発メモリ内コンフィグなし
		init_system_status(0);
	}else{
		init_system_status(&cfg);
	}

	init_power_management_task(&system_status,&volume_sw);
	task_id_table.power_management_task_id=create_task(&application_task_list,power_management_task,0);

	normal_mode_view_init(&system_status);
	task_id_table.normal_view_task_id=create_task(&application_task_list,normal_mode_view_task,normal_mode_view_on_activated);

	ac_status_view_task_init(&(system_status.power_status));
	task_id_table.ac_status_view_task_id=create_task(&application_task_list,ac_status_view_task,0);

	battery_management_task_init(&(system_status.power_status));
	task_id_table.battery_management_task_id=create_task(&application_task_list,battery_management_task,0);

	normal_vfo_encoder_task_init(&system_status);
	task_id_table.normal_vfo_encoder_task_id=create_task(&application_task_list,normal_vfo_encoder_task,0);

	vfo_output_task_init(&system_status);
	task_id_table.vfo_output_task_id=create_task(&application_task_list,vfo_output_task,0);

	radio_circuit_control_task_init(&system_status);
	task_id_table.radio_circuit_ctrl_task_id=create_task(&application_task_list,radio_circuit_control_task,0);

	ptt_observe_normal_task_init(&system_status,&ptt);
	task_id_table.ptt_observe_normal_task_id=create_task(&application_task_list,ptt_observe_normal_task,ptt_observe_normal_task_on_activated);

	rssi_observe_task_init(&system_status);
	task_id_table.rssi_observe_task_id=create_task(&application_task_list,rssi_observe_task,0);

	userinput_observe_normal_task_init(&system_status,&application_task_list,&task_id_table);
	task_id_table.userinput_observe_normal_task_id=create_task(&application_task_list,userinput_observe_normal_task,0);

	userinput_observe_calibration_task_init(&system_status,&application_task_list,&task_id_table);
	task_id_table.userinput_observe_calibration_task_id=create_task(&application_task_list,userinput_observe_calibration_task,0);

	calibration_view_task_init(&system_status,&application_task_list,&task_id_table);
	task_id_table.calibration_view_task_id=create_task(&application_task_list,calibration_view_task,calibration_view_task_on_activated);

	xtal_calibraion_encoder_task_init(&system_status);
	task_id_table.xtal_calibration_encoder_task_id=create_task(&application_task_list,xtal_calibration_encoder_task,0);

	if_calibration_encoder_task_init(&system_status);
	task_id_table.if_calibration_encoder_task_id=create_task(&application_task_list,if_calibration_encoder_task,0);

	normal_rit_encoder_task_init(&system_status);
	task_id_table.normal_rit_encoder_task_id=create_task(&application_task_list,normal_rit_encoder_task,0);

	ptt_observe_calibration_mode_task_init(&system_status,&ptt);
	task_id_table.ptt_calibration_mode_task_id=create_task(&application_task_list,ptt_observe_calibration_mode_task,ptt_observe_calibration_mode_task_on_activated);

	//activate common task
	activate_task(&application_task_list,task_id_table.power_management_task_id);
	activate_task(&application_task_list,task_id_table.ac_status_view_task_id);
	activate_task(&application_task_list,task_id_table.battery_management_task_id);
	activate_task(&application_task_list,task_id_table.radio_circuit_ctrl_task_id);
	activate_task(&application_task_list,task_id_table.vfo_output_task_id);
	activate_task(&application_task_list,task_id_table.rssi_observe_task_id);
	if(config_load_result==0){//不揮発メモリから設定が読めなかったらキャリブレーションモードで起動する
		switch_to_calibration_mode(&application_task_list,&task_id_table);
	}else{
		switch_to_normal_mode(&application_task_list,&task_id_table);
	}

}

void switch_to_calibration_mode(TASK_LIST_t*t,APPLICATION_TASK_ID_TABLE_t*t_ids){
	deactivate_task(t,t_ids->normal_rit_encoder_task_id);
	deactivate_task(t,t_ids->normal_view_task_id);
	deactivate_task(t,t_ids->normal_vfo_encoder_task_id);
	deactivate_task(t,t_ids->ptt_observe_normal_task_id);
	deactivate_task(t,t_ids->userinput_observe_normal_task_id);
	activate_task(t,t_ids->calibration_view_task_id);
	activate_task(t,t_ids->xtal_calibration_encoder_task_id);
	activate_task(t,t_ids->userinput_observe_calibration_task_id);
	activate_task(t,t_ids->ptt_calibration_mode_task_id);
}
void switch_to_normal_mode(TASK_LIST_t*t,APPLICATION_TASK_ID_TABLE_t*t_ids){
	activate_task(t,t_ids->normal_view_task_id);
	activate_task(t,t_ids->normal_vfo_encoder_task_id);
	activate_task(t,t_ids->ptt_observe_normal_task_id);
	activate_task(t,t_ids->userinput_observe_normal_task_id);
	deactivate_task(t,t_ids->calibration_view_task_id);
	deactivate_task(t,t_ids->xtal_calibration_encoder_task_id);
	deactivate_task(t,t_ids->if_calibration_encoder_task_id);
	deactivate_task(t,t_ids->userinput_observe_calibration_task_id);
	deactivate_task(t,t_ids->ptt_calibration_mode_task_id);
}
void process_application(void){
	do_task(&application_task_list);
}
