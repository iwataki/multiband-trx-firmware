/*
 * draw_system_status.c
 *
 *  Created on: 2020/03/17
 *      Author: �@��Y
 */


#include "draw_system_status.h"
#include "aqm1248.h"
//for debug
//TODO: move to separate file
void print_text(uint8_t x,uint8_t y,uint8_t size, uint8_t s[]){
	if(s==0){//null pointer
		return;
	}
	for(int i=0;s[i]!=0;i++){
		uint8_t char_pos=x+6*i*size;
		draw_char(char_pos,y,size,s[i]);
	}
}
