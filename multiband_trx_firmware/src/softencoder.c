/*
 * softencoder.c
 *
 *  Created on: 2012/03/25
 *      Author: ���@�@��Y
 */
#include <avr/io.h>
#include <avr/delay.h>
#include "softencoder.h"

const int enc_table[16]={0,-1,1,2,1,0,2,-1,-1,2,0,1,2,1,-1,0};


void init_enc(ENCODER_t*enc,int dirc){
	*(enc->DDR_phaseA)&=~(1<<enc->bitA);
	*(enc->DDR_phaseB)&=~(1<<enc->bitB);
	*(enc->PORT_phaseA)|=(1<<enc->bitA);
	*(enc->PORT_phaseB)|=(1<<enc->bitB);
	enc->direction=dirc;
	enc->latest_count=0;
	enc->prev_count=0;
	enc->prev_portval=0;
	enc->prev_direction=0;
}
void set_encoder_val(ENCODER_t*enc,int val){
	enc->latest_count=val;
	enc->prev_count=val;
}
int get_encoder_val(ENCODER_t*enc){
	uint16_t latest=enc->latest_count;
	enc->prev_count=enc->latest_count;
	return enc->latest_count;
}

int get_encoder_diff(ENCODER_t*enc){
	int16_t diff=enc->latest_count-enc->prev_count;
	enc->prev_count=enc->latest_count;
	return diff;

}

void update_encoder(ENCODER_t*enc){
	int portval=0;
	if(enc->bitA!=0){
		portval=((*(enc->PIN_phaseA)>>(enc->bitA-1))&0x02)|((*(enc->PIN_phaseB)>>enc->bitB)&0x01);
	}else{
		portval=((*(enc->PIN_phaseA)<<1)&0x02)|((*(enc->PIN_phaseB)>>enc->bitB)&0x01);
	}
	if(enc_table[portval<<2|enc->prev_portval]==2){
		enc->latest_count+=enc->direction*enc->prev_direction*2;
	}else{
		enc->latest_count+=enc->direction*enc_table[portval<<2|enc->prev_portval];
		enc->prev_direction=enc->direction*enc_table[portval<<2|enc->prev_portval];
	}
	enc->prev_portval=portval;
}
