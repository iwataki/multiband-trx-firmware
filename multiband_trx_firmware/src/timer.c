#include <avr/io.h>
#include <avr/interrupt.h>
#include "timer.h"

void(*p_called_func)(void);
void timer_init(unsigned int period,void(*p_func)(void)){
	p_called_func=p_func;
	TIMSK1|=_BV(OCIE1A);
	TCCR1A=0x00;
	TCCR1C=0x00;
	if(period<=65){
		OCR1A=500*period-1;
		TCCR1B=0x0a;//prescale 8
	}else if((period>16)&&(period<=520)){
		OCR1A=125*period-1;
		TCCR1B=0x0b;//prescale 64
	}else if((period>520)&&(period<=1048)){
		OCR1A=62*period-1;
		TCCR1B=0x0c;//prescale 256
	}
	sei();
}
void timer_init_us(unsigned int period,void(*p_func)(void)){//for 16mhz
	p_called_func=p_func;
	TIMSK1|=_BV(OCIE1A);
	TCCR1A=0x00;
	TCCR1C=0x00;
	OCR1A=period-1;
	TCCR1B=0x0a;//div8
	sei();
}
ISR(TIMER1_COMPA_vect){
	p_called_func();
}
	
