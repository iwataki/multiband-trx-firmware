/*
 * battery_management_task.c
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */
#include "hardware.h"
#include "system_constants.h"
#include "battery_management_task.h"
#define BATTERY_MANAGEMENT_STATE_BATTERY_POWERED 0
#define BATTERY_MANAGEMENT_STATE_AC_POWERED 1
#define BATTERY_MANAGEMENT_STATE_BATTERY_CHARGING 2

static POWER_STATE_t*power_state;
static uint8_t battery_management_state;
void battery_management_task_init(POWER_STATE_t*power_state_){
	power_state=power_state_;
	battery_management_state=BATTERY_MANAGEMENT_STATE_BATTERY_POWERED;
	battery_management_task();
}

uint8_t on_bmt_ac_powered(uint8_t is_ac_pres,uint16_t battery_voltage);
uint8_t on_bmt_battery_powered(uint8_t is_ac_pres,uint16_t battery_voltage);
uint8_t on_bmt_battery_charging(uint8_t is_ac_pres,uint16_t battery_voltage);

void battery_management_task(void){
	uint8_t is_ac_presense=!get_switch(&ac_presense_input);
	uint8_t is_power_alarmed=!get_switch(&power_alarm_input);
	uint16_t battery_voltage=((uint32_t)adc_get(&battery_level_adc))*514/100;

	power_state->voltage=battery_voltage;

	//state transition
	uint8_t new_state;
	switch(battery_management_state){
	case BATTERY_MANAGEMENT_STATE_AC_POWERED:
		new_state=on_bmt_ac_powered(is_ac_presense,battery_voltage);
		break;
	case BATTERY_MANAGEMENT_STATE_BATTERY_CHARGING:
		new_state=on_bmt_battery_charging(is_ac_presense,battery_voltage);
		break;
	case BATTERY_MANAGEMENT_STATE_BATTERY_POWERED:
		new_state=on_bmt_battery_powered(is_ac_presense,battery_voltage);
		break;
	}
	battery_management_state=new_state;
	//determine output
	switch(battery_management_state){
	case BATTERY_MANAGEMENT_STATE_AC_POWERED:
		digitalout_set(&acinput_select_pin);
		digitalout_reset(&battery_chaege_enable_pin);
		power_state->is_ac_powered=1;
		power_state->is_charging=0;
		break;
	case BATTERY_MANAGEMENT_STATE_BATTERY_CHARGING:
		digitalout_set(&battery_chaege_enable_pin);
		digitalout_set(&acinput_select_pin);
		power_state->is_charging=1;
		power_state->is_ac_powered=1;
		break;
	case BATTERY_MANAGEMENT_STATE_BATTERY_POWERED:
		digitalout_reset(&acinput_select_pin);
		digitalout_reset(&battery_chaege_enable_pin);
		power_state->is_ac_powered=0;
		power_state->is_charging=0;
		break;
	}

}

uint8_t on_bmt_ac_powered(uint8_t is_ac_pres,uint16_t battery_voltage){
	if(is_ac_pres==0){
		return BATTERY_MANAGEMENT_STATE_BATTERY_POWERED;
	}
	if((battery_voltage<battery_voltage_threshold[3])&&(battery_voltage>5000)){
		return BATTERY_MANAGEMENT_STATE_BATTERY_CHARGING;
	}
	return BATTERY_MANAGEMENT_STATE_AC_POWERED;

}
uint8_t on_bmt_battery_powered(uint8_t is_ac_pres,uint16_t battery_voltage){
	if(is_ac_pres){
		if((battery_voltage<battery_voltage_threshold[3])&&(battery_voltage>5000)){
			return BATTERY_MANAGEMENT_STATE_BATTERY_CHARGING;
		}
		return BATTERY_MANAGEMENT_STATE_AC_POWERED;
	}
	return BATTERY_MANAGEMENT_STATE_BATTERY_POWERED;
}
uint8_t on_bmt_battery_charging(uint8_t is_ac_pres,uint16_t battery_voltage){
	if(!is_ac_pres){
		return BATTERY_MANAGEMENT_STATE_BATTERY_POWERED;
	}
	if(battery_voltage>battery_voltage_threshold[4]){
		return BATTERY_MANAGEMENT_STATE_AC_POWERED;
	}
	return BATTERY_MANAGEMENT_STATE_BATTERY_CHARGING;
}
