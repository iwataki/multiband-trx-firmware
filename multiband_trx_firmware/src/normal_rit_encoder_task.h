/*
 * normal_rit_encoder_task.h
 *
 *  Created on: 2020/03/20
 *      Author: �@��Y
 */

#ifndef NORMAL_RIT_ENCODER_TASK_H_
#define NORMAL_RIT_ENCODER_TASK_H_

#include "data_structure.h"

void normal_rit_encoder_task_init(SYSTEM_STATUS_t*s);
void normal_rit_encoder_task();

#endif /* NORMAL_RIT_ENCODER_TASK_H_ */
