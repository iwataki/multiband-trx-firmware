/*
 * system_constants.h
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */

#ifndef SYSTEM_CONSTANTS_H_
#define SYSTEM_CONSTANTS_H_

#include <inttypes.h>
#define DEFAULT_IF_FREQ 10700000
#define DEFAULT_XTAL_FREQ 25000000
#define DEFAULT_VFO_FREQ 50500000
#define VFO_FREQ_MAX 51000000
#define VFO_FREQ_MIN 50000000

#define RIT_FREQ_MAX 900
#define RIT_FREQ_MIN -900

#define VFO_RX_CHANNEL 0
#define VFO_TX_CHANNEL 1

#define BATTERY_CHARGE_COMPLETE_CURRENT 8

extern const uint16_t battery_voltage_threshold[];


#endif /* SYSTEM_CONSTANTS_H_ */
