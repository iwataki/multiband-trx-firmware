/*
 * userinput_observe_calibration_task.c
 *
 *  Created on: 2020/03/20
 *      Author: �@��Y
 */

#include "userinput_observe_calibration_task.h"
#include "hardware.h"
#include "task.h"
#include "calibration_target.h"

static TASK_LIST_t*task_list;
static APPLICATION_TASK_ID_TABLE_t*application_tasks;
static SYSTEM_STATUS_t*status;

void change_step_cal(CALIBRATION_DATA_t*cfg){
	uint8_t step=cfg->step;
	if(step==1){
		step=10;
	}else if(step==10){
		step=100;
	}else if(step==100){
		step=1;
	}
	cfg->step=step;
}
void userinput_observe_calibration_task_init(SYSTEM_STATUS_t*s,TASK_LIST_t*t,APPLICATION_TASK_ID_TABLE_t*t_ids){
	task_list=t;
	application_tasks=t_ids;
	status=s;
}


void userinput_observe_calibration_task(void){
	uint8_t left_sw_event=get_switch_event(&left_button);
	uint8_t center_sw_event=get_switch_event(&center_button);
	uint8_t right_sw_event=get_switch_event(&right_button);

	if(left_sw_event==USERSW_PUSHED){//end calibration mode
		switch_to_normal_mode(task_list,application_tasks);
	}
	if(center_sw_event==USERSW_PUSHED){//switch calibration target
		uint8_t cal_target=get_cal_target(task_list,application_tasks);
		if(cal_target==CAL_TARGET_IF){
			set_cal_target(task_list,application_tasks,CAL_TARGET_XTAL);
		}else{
			set_cal_target(task_list,application_tasks,CAL_TARGET_IF);
		}
	}
	if(right_sw_event==USERSW_PUSHED){//change step
		change_step_cal(&(status->calibration));
	}
}
