/*
 * ac_status_view_task.h
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */

#ifndef AC_STATUS_VIEW_TASK_H_
#define AC_STATUS_VIEW_TASK_H_

#include "data_structure.h"
#include "textbox.h"
#include "battery_icon.h"
#include "ac_icon.h"

typedef struct{
	TEXTBOX_t battery_voltage;
	BATTERY_ICON_t battery_icon;
	AC_ICON_t ac_icon;
}AC_STATUS_VIEW_t;

void ac_status_view_task_init(POWER_STATE_t*state);
void ac_status_view_task(void);

#endif /* AC_STATUS_VIEW_TASK_H_ */
