/*
 * textbox.c
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */

#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include "textbox.h"
#include "draw_system_status.h"

void textbox_create(TEXTBOX_t*textbox,uint8_t x,uint8_t y,uint8_t width,uint8_t size){
	uint8_t chars=width/(6*size)+1;//width based text count + \0
	textbox->text=(int8_t*)malloc(chars);
	textbox->size=size;
	textbox->text_capacity=chars;
	textbox->x=x;
	textbox->y=y;
	memset(textbox->text,0,chars);
}
void textbox_printf(TEXTBOX_t*textbox,int8_t*fmt,...){
	va_list arg;
	va_start(arg,fmt);
	vsnprintf(textbox->text,textbox->text_capacity,fmt,arg);
	va_end(arg);
	print_text(textbox->x,textbox->y,textbox->size,textbox->text);
}
