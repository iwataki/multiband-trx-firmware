/*
 * ptt_observe_normal_task.c
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */
#include "ptt_observe_normal_task.h"


static SYSTEM_STATUS_t*status;
static USERSW_t*ptt;
void ptt_observe_normal_task_init(SYSTEM_STATUS_t*s,USERSW_t*p){
	status=s;
	ptt=p;
}

void ptt_observe_normal_task_on_activated(){
	status->radio_circuit_status.is_receiver_enable=1;
	status->radio_circuit_status.is_transmitter_enable=0;
}
void ptt_observe_normal_task(void){
	uint8_t e=get_switch_event(ptt);
	if(e==USERSW_HOLD){
		status->radio_circuit_status.is_receiver_enable=0;
		status->radio_circuit_status.is_transmitter_enable=1;
	}
	if(e==USERSW_RELEASE){
		status->radio_circuit_status.is_receiver_enable=1;
		status->radio_circuit_status.is_transmitter_enable=0;
	}
}

