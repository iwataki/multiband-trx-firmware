/*
 * ptt_observe_normal_task.h
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */

#ifndef PTT_OBSERVE_NORMAL_TASK_H_
#define PTT_OBSERVE_NORMAL_TASK_H_

#include "user_switch.h"
#include "data_structure.h"

void ptt_observe_normal_task_init(SYSTEM_STATUS_t*s,USERSW_t*ptt);
void ptt_observe_normal_task_on_activated();
void ptt_observe_normal_task(void);


#endif /* PTT_OBSERVE_NORMAL_TASK_H_ */
