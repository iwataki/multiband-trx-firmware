/*
 * vfo_control.h
 *
 *  Created on: 2020/03/22
 *      Author: �@��Y
 */

#ifndef VFO_CONTROL_H_
#define VFO_CONTROL_H_
#include "data_structure.h"

void apply_vfo_state(VFO_CONFIG_t*vfo,RADIO_CIRCUIT_POWER_STATE_t*radio,CALIBRATION_DATA_t*cal);
#endif /* VFO_CONTROL_H_ */
