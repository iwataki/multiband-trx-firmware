/*
 * application.h
 *
 *  Created on: 2020/03/18
 *      Author: 宗一郎
 */

#ifndef APPLICATION_H_
#define APPLICATION_H_
#include <inttypes.h>
#include "task.h"

typedef struct {
	int power_management_task_id;
	int normal_view_task_id;
	int ac_status_view_task_id;
	int battery_management_task_id;
	int normal_vfo_encoder_task_id;
	int vfo_output_task_id;
	int radio_circuit_ctrl_task_id;
	int ptt_observe_normal_task_id;
	int rssi_observe_task_id;
	int userinput_observe_normal_task_id;//ここから下，要実装
	int userinput_observe_calibration_task_id;
	int calibration_view_task_id;
	int xtal_calibration_encoder_task_id;
	int if_calibration_encoder_task_id;
	int normal_rit_encoder_task_id;
	int ptt_calibration_mode_task_id;
}APPLICATION_TASK_ID_TABLE_t;
void switch_to_calibration_mode(TASK_LIST_t*t,APPLICATION_TASK_ID_TABLE_t*t_ids);
void switch_to_normal_mode(TASK_LIST_t*t,APPLICATION_TASK_ID_TABLE_t*t_ids);
void init_application(void);
void process_application(void);

#endif /* APPLICATION_H_ */
