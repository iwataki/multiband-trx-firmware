/*
 * rssi_observe_task.h
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */

#ifndef RSSI_OBSERVE_TASK_H_
#define RSSI_OBSERVE_TASK_H_

#include "data_structure.h"

void rssi_observe_task_init(SYSTEM_STATUS_t*s);
void rssi_observe_task(void);

#endif /* RSSI_OBSERVE_TASK_H_ */
