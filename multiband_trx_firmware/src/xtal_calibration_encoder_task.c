/*
 * xtal_calibration_encoder_task.c
 *
 *  Created on: 2020/03/20
 *      Author: �@��Y
 */


#include "xtal_calibration_encoder_task.h"
#include "hardware.h"

static SYSTEM_STATUS_t*status;
static int diff_incr=0;
void xtal_calibraion_encoder_task_init(SYSTEM_STATUS_t*s){
	status=s;
}
void xtal_calibration_encoder_task(void){
	diff_incr+=get_encoder_diff(&encoder);
	int diff_half=diff_incr/2;
	if(diff_half!=0){
		diff_incr=0;
	}
	int f_change=diff_half*status->calibration.step;
	status->calibration.crystal+=f_change;
}
