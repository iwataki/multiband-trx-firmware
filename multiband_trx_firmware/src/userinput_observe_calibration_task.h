/*
 * userinput_observe_calibration_task.h
 *
 *  Created on: 2020/03/20
 *      Author: �@��Y
 */

#ifndef USERINPUT_OBSERVE_CALIBRATION_TASK_H_
#define USERINPUT_OBSERVE_CALIBRATION_TASK_H_

#include "application.h"
#include "data_structure.h"


void userinput_observe_calibration_task_init(SYSTEM_STATUS_t*s,TASK_LIST_t*t,APPLICATION_TASK_ID_TABLE_t*t_ids);
void userinput_observe_calibration_task(void);


#endif /* USERINPUT_OBSERVE_CALIBRATION_TASK_H_ */
