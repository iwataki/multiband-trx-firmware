/*
 * radio_circuit_control_task.c
 *
 *  Created on: 2020/03/19
 *      Author: �@��Y
 */

#include "radio_circuit_control_task.h"
#include "hardware.h"

static SYSTEM_STATUS_t*status;
void radio_circuit_control_task_init(SYSTEM_STATUS_t*s){
	status=s;
	radio_circuit_control_task();
}
void radio_circuit_control_task(void){
	if(status->radio_circuit_status.is_receiver_enable!=0){
		digitalout_set(&receiver_enable_pin);
	}else{
		digitalout_reset(&receiver_enable_pin);
	}
	if(status->radio_circuit_status.is_transmitter_enable!=0){
		digitalout_set(&transmitter_enable_pin);
	}else{
		digitalout_reset(&transmitter_enable_pin);
	}
}
