/*
 * hardware.h
 *
 *  Created on: 2020/03/16
 *      Author: �@��Y
 */

#ifndef HARDWARE_H_
#define HARDWARE_H_
#include "user_switch.h"
#include "softencoder.h"
#include "adc.h"
#include "digitalout.h"

extern USERSW_t left_button;
extern USERSW_t center_button;
extern USERSW_t right_button;
extern ENCODER_t encoder;
extern USERSW_t volume_sw;
extern USERSW_t ptt;
extern ADC_t battery_level_adc;
extern ADC_t rssi_adc;
extern ADC_t battery_charge_adc;
extern DIGITALOUT_t battery_chaege_enable_pin;
extern DIGITALOUT_t acinput_select_pin;
extern USERSW_t ac_presense_input;
extern USERSW_t power_alarm_input;
extern DIGITALOUT_t transmitter_enable_pin;
extern DIGITALOUT_t receiver_enable_pin;

void init_hardware(void);

void update_hardware(void);//called from timer routine

#endif /* HARDWARE_H_ */
