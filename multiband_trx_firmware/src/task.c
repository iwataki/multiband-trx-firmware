/*
 * task.c
 *
 *  Created on: 2020/03/16
 *      Author: �@��Y
 */

#include <stdlib.h>
#include "task.h"



void init_task(TASK_LIST_t*tasks){
	tasks->head=NULL;
	tasks->tail=NULL;
	tasks->count=0;
}
int create_task(TASK_LIST_t*tasks,void(*process)(void),void(*on_activated)(void)){
	TCB_t*new_task=(TCB_t*)malloc(sizeof(TCB_t));
	if(new_task==NULL){//severe eror
		return -1;
	}
	new_task->previous_status=TASK_INACTIVE;
	new_task->status=TASK_INACTIVE;
	new_task->process=process;
	new_task->on_activated=on_activated;
	new_task->task_id=tasks->count;
	new_task->next=NULL;
	if(tasks->head==NULL){
		tasks->head=new_task;
		tasks->tail=new_task;
	}else{
		new_task->prev=tasks->tail;
		tasks->tail->next=new_task;
		tasks->tail=new_task;

	}
	tasks->count++;
	return new_task->task_id;

}

void set_task_status(TASK_LIST_t*tasks,int task_id,int new_state){
	TCB_t*now=tasks->head;
	while(now!=NULL){
		if(now->task_id==task_id){
			now->previous_status=now->status;
			now->status=new_state;
		}
		now=now->next;
	}

}
void activate_task(TASK_LIST_t*tasks,int task_id){
	set_task_status(tasks,task_id,TASK_ACTIVE);
}
void deactivate_task(TASK_LIST_t*tasks,int task_id){
	set_task_status(tasks,task_id,TASK_INACTIVE);
}
void do_task(TASK_LIST_t*tasks){
	TCB_t*now=tasks->head;
	while(now!=NULL){
		if(now->status==TASK_ACTIVE){
			if(now->previous_status==TASK_INACTIVE){
				if(now->on_activated!=0){
					now->on_activated();
				}
			}
			now->process();
		}
		now->previous_status=now->status;
		now=now->next;
	}

}
uint8_t get_task_status(TASK_LIST_t*tasks,int task_id){
	uint8_t s=TASK_INACTIVE;
	TCB_t*now=tasks->head;
	while(now!=NULL){
		if(now->task_id==task_id){
			s=now->status;
		}
		now=now->next;
	}
	return s;
}
