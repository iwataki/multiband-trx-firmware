/*
 * batter_icon.h
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */

#ifndef BATTERY_ICON_H_
#define BATTERY_ICON_H_

#include <inttypes.h>

#define BATTERY_ICON_FULL 4
#define BATTERY_ICON_75 3
#define BATTERY_ICON_50 2
#define BATTERY_ICON_25 1
#define BATTERY_ICON_0 0

typedef struct{
	uint8_t x;
	uint8_t y;
}BATTERY_ICON_t;

void battery_icon_create(BATTERY_ICON_t*icon,uint8_t x,uint8_t y);
void battery_icon_setvalue(BATTERY_ICON_t*icon,uint8_t level);


#endif /* BATTERY_ICON_H_ */
