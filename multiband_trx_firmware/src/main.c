/*
 * main.c
 *
 *  Created on: 2020/03/16
 *      Author: �@��Y
 */

#include "hardware.h"
#include "timer.h"
#include "draw_system_status.h"
#include "power_management.h"
#include "application.h"
#include "aqm1248.h"
#include <util/delay.h>

void boot(){
	system_poweron();
	_delay_ms(5);
	init_hardware();
	clear_screen();
	init_application();
	timer_init(10,update_hardware);
}

int main(void){
	boot();

	while(1){
		process_application();
		system_idle();
	}
	return 0;
}
