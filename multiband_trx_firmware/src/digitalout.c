/*
 * digitalout.c
 *
 *  Created on: 2020/03/18
 *      Author: �@��Y
 */


#include "digitalout.h"

void init_digitalout(DIGITALOUT_t*d){
	*(d->DDR)|=(1<<(d->bit));
}
void digitalout_set(DIGITALOUT_t*d){
	*(d->PORT)|=(1<<(d->bit));
}
void digitalout_reset(DIGITALOUT_t*d){
	*(d->PORT)&=~(1<<(d->bit));
}
